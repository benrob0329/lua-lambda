local load = loadstring or load

local lambda_template = "return function %s return %s end"
local cache = {}

-- Lambda Format: l"(args) exp"
local function l(s)
	if not cache[s] then
		cache[s] = load(lambda_template:format(s:match("(%b()) (.+)$")))()
	end
	return cache[s]
end

-- Tests --
do
	local x = "Test"
	local f = l"(x) x"
	assert(f(x) == x, "Lambda Test Failed!")
end
do
	local t = {1,2,3,4,5,6,7,8,9}
	table.sort(t, l"(a,b) a > b")
	assert(t[1] == 9, "Lamba Test Failed!")
end

return l
