# lua-lambda
Takes a string, returns a function for quick function definition.

## Usage
The lambda format is as follows:
```Lua
l"(args) expr"
```

Example:
```Lua
local l = require("lambda") -- Omit this in Minetest mods

local add_one = l"(x) x+1"
print(add_one(5)) --> 6
```

## Installation
### Luarocks
```Bash
luarocks install lambda
```

### Minetest
This module is available on Minetest's [ContentDB](https://content.minetest.net/), simple add `lambda` as a dependancy to your `mod.conf` and Minetest will automatically install the module for end-users. If you wish to manually install this, then you can either clone the repositiory and add it to your `mods/` folder or download it [here](https://content.minetest.net/packages/benrob0329/lambda/).

## Why?
Because Lua needs more atomic modules which add functionality without creating a huge library. This module is supposed to be dead-simple to grab and start using in your project without adding anything extra. One of Lua's strengths is minimalism, and I think that it's ecosystem would do well to mirror that.
